package az.card.mscard.mapper;

import az.card.mscard.dao.entity.CardEntity;
import az.card.mscard.model.dto.CardDto;
import org.mapstruct.*;

import static az.card.mscard.util.EncryptionUtil.decrypt;

@Mapper(componentModel = "spring")
public interface CardMapper {

    @Mapping(target = "cardNumber", source = "encryptedCardNumber")
    CardDto toCardDto(CardEntity cardEntity);

    @AfterMapping
    default void setDecryptedFields(@MappingTarget CardDto cardDto) {
        if (cardDto != null) {
            cardDto.setCardNumber(decrypt(cardDto.getCardNumber()));
            cardDto.setExpirationDate(decrypt(cardDto.getExpirationDate()));
            cardDto.setCvv(decrypt(cardDto.getCvv()));
        }
    }
}
