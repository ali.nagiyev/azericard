package az.card.mscard.dao.repository;

import az.card.mscard.dao.entity.CardEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CardRepository extends JpaRepository<CardEntity, Long> {

    List<CardEntity> findAllByUserUuid(String userUUID, Pageable pageable);

    Optional<CardEntity> findByCardUuid(String cardUuid);

    List<CardEntity> findAllByCardUuidIn(List<String> cardUUIDs);
}
