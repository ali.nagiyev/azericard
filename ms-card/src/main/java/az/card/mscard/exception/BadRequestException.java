package az.card.mscard.exception;

import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class BadRequestException extends RuntimeException {

    String code;

    public BadRequestException(String message, String code) {
        super(message);
        this.code = code;
    }
}
