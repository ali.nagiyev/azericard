package az.card.mscard.mapper;

import az.card.mscard.dao.entity.CardEntity;
import az.card.mscard.model.dto.CardDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-29T14:47:08+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.3.jar, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class CardMapperImpl implements CardMapper {

    @Override
    public CardDto toCardDto(CardEntity cardEntity) {
        if ( cardEntity == null ) {
            return null;
        }

        CardDto cardDto = new CardDto();

        cardDto.setCardNumber( cardEntity.getEncryptedCardNumber() );
        cardDto.setExpirationDate( cardEntity.getExpirationDate() );
        cardDto.setCvv( cardEntity.getCvv() );
        cardDto.setBalance( cardEntity.getBalance() );

        setDecryptedFields( cardDto );

        return cardDto;
    }
}
