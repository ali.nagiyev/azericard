<h1 align="center"> ms.card </h1> <br>

## Table of Contents

- [Introduction](#Introduction)
- [How to run locally](#How-to-run-locally)
- [Tech stack](#Tech-stack)
- [Swagger](#Swagger-link)

## Introduction

This app's responsibility is to register user's cards and register them. When you purchase something from products , then balance decrease automatically

## How to run locally

shell script
$ java -jar ms.card.jar


## Tech stack
1. Spring Boot
2. Java 11

## Swagger link
[ms.auth](http://localhost:8089/swagger-ui/index.html#/
