package az.auth.msauth.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

import static az.auth.msauth.model.constant.ExceptionConstants.BAD_INPUT_CODE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(BAD_REQUEST)
    public ExceptionResponse handle(BadRequestException ex) {
        return ExceptionResponse.of(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public ExceptionResponse handle(MethodArgumentNotValidException ex) {

        var logMessage = ex.getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .reduce((s1, s2) -> String.format("%s, %s", s1, s2))
                .orElse("");

        logger.error("MethodArgumentNotValidException: {}", logMessage);
        return ExceptionResponse.of(BAD_INPUT_CODE, ex.getFieldErrors().get(0).getDefaultMessage());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(BAD_REQUEST)
    public ExceptionResponse handle(ConstraintViolationException ex) {

        logger.error("ConstraintViolationException: {}", ExceptionUtils.getStackTrace(ex));
        return ExceptionResponse.of(BAD_INPUT_CODE, ex.getMessage());
    }
}
