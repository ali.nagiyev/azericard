package az.auth.msauth.mapper.factory;

import az.auth.msauth.model.jwt.AccessTokenClaimsSet;
import az.auth.msauth.model.jwt.RefreshTokenClaimsSet;
import lombok.NoArgsConstructor;

import java.util.Date;

import static java.time.LocalDateTime.now;
import static jodd.time.TimeUtil.toDate;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class TokenBuilder {

    public static AccessTokenClaimsSet buildAccessTokenClaimsSet(String userName, String userUUID, Date expirationTime) {

        return AccessTokenClaimsSet.builder()
                .userName(userName)
                .userUUID(userUUID)
                .createdTime(toDate(now()))
                .expirationTime(expirationTime)
                .build();
    }

    public static RefreshTokenClaimsSet buildRefreshTokenClaimsSet(String userName, String userUUID, int refreshTokenExpirationCount, Date expirationTime) {

        return RefreshTokenClaimsSet.builder()
                .userName(userName)
                .userUUID(userUUID)
                .expirationTime(expirationTime)
                .count(refreshTokenExpirationCount)
                .build();
    }
}
