package az.auth.msauth.model.constant;

public interface Headers {

    String LANGUAGE = "Accept-Language";
    String AUTHORIZATION = "Authorization";
    String REFRESH_TOKEN = "refresh-Token";
    String INITIAL_TOKEN = "initial-token";
}
