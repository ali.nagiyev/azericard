package az.auth.msauth.model.constant;

public interface ExceptionConstants {

    String USER_UNAUTHORIZED_CODE = "USER_UNAUTHORIZED";
    String USER_UNAUTHORIZED_MESSAGE = "User unauthorized";
    String TOKEN_EXPIRED_CODE = "TOKEN_EXPIRED";
    String TOKEN_EXPIRED_MESSAGE = "Token expired";
    String BAD_INPUT_CODE = "BAD_INPUT";
}