package az.auth.msauth.model.constant;

public interface AuthConstants {
    String RSA = "RSA";
    String AUTH_CACHE_DATA_PREFIX = "USER_SESSION:";
    int KEY_SIZE = 2048;
    Long TOKEN_EXPIRE_DAY_COUNT = 30L;
}
