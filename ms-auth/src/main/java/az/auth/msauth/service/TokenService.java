package az.auth.msauth.service;

import az.auth.msauth.exception.AuthException;
import az.auth.msauth.model.jwt.AuthCacheData;
import az.auth.msauth.model.jwt.RefreshTokenClaimsSet;
import az.auth.msauth.model.response.AuthPayloadResponse;
import az.auth.msauth.model.response.TokenResponse;
import az.auth.msauth.util.CacheUtil;
import az.auth.msauth.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;

import static az.auth.msauth.mapper.factory.TokenBuilder.buildAccessTokenClaimsSet;
import static az.auth.msauth.mapper.factory.TokenBuilder.buildRefreshTokenClaimsSet;
import static az.auth.msauth.model.constant.AuthConstants.*;
import static az.auth.msauth.model.constant.ExceptionConstants.*;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.springframework.util.Base64Utils.decodeFromString;
import static org.springframework.util.Base64Utils.encodeToString;

@Slf4j
@Service
@RequiredArgsConstructor
public class TokenService {

    private final JwtUtil jwtUtil;
    private final CacheUtil cacheUtil;

    @Value("${jwt.accessToken.expiration.time}")
    private int accessTokenExpirationTime;

    @Value("${jwt.refreshToken.expiration.time}")
    private int refreshTokenExpirationTime;

    public TokenResponse generateToken(String userName, String userUUID, int refreshTokenExpirationCount) {

        var accessTokenClaimsSet = buildAccessTokenClaimsSet(
                userName,
                userUUID,
                jwtUtil.generateSessionExpirationTime(accessTokenExpirationTime)
        );

        var refreshTokenClaimsSet = buildRefreshTokenClaimsSet(
                userName,
                userUUID,
                refreshTokenExpirationCount,
                jwtUtil.generateSessionExpirationTime(refreshTokenExpirationTime)
        );

        var keyPair = jwtUtil.generateKeyPair();

        var authCacheData = AuthCacheData.of(
                accessTokenClaimsSet,
                encodeToString(keyPair.getPublic().getEncoded())
        );

        cacheUtil.saveToCache(AUTH_CACHE_DATA_PREFIX + userName + ":" + userUUID, authCacheData, TOKEN_EXPIRE_DAY_COUNT, DAYS);

        var accessToken = jwtUtil.generateToken(accessTokenClaimsSet, keyPair.getPrivate());
        var refreshToken = jwtUtil.generateToken(refreshTokenClaimsSet, keyPair.getPrivate());

        return TokenResponse.of(accessToken, refreshToken);
    }

//    public TokenResponse refreshTokens(String refreshToken) {
//
//        RefreshTokenClaimsSet refreshTokenClaimsSet = jwtUtil.getClaimsFromRefreshToken(refreshToken);
//        var userName = refreshTokenClaimsSet.getUserName();
//        var userUUID = refreshTokenClaimsSet.getUserUUID();
//
//        try {
//
//            if (authCacheData == null) throw new AuthException(USER_UNAUTHORIZED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);
//
//            var publicKey = KeyFactory.getInstance(RSA).generatePublic(
//                    new X509EncodedKeySpec(decodeFromString(authCacheData.getPublicKey()))
//            );
//
//            jwtUtil.verifyToken(refreshToken, (RSAPublicKey) publicKey);
//
//            if (jwtUtil.isRefreshTokenTimeExpired(refreshTokenClaimsSet)) {
//                throw new AuthException(REFRESH_TOKEN_EXPIRED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);
//            }
//
//            if (jwtUtil.isRefreshTokenCountExpired(refreshTokenClaimsSet)) {
//                throw new AuthException(REFRESH_TOKEN_COUNT_EXPIRED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);
//            }
//
//            return generateToken(accountId, deviceId, refreshTokenClaimsSet.getCount() - 1);
//        } catch (AuthException ex) {
//            throw ex;
//        } catch (Exception ex) {
//            throw new AuthException(USER_UNAUTHORIZED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);
//        }
//    }
//
    public AuthPayloadResponse validateToken(String accessToken) {

        try {
            var userName = jwtUtil.getClaimsFromAccessToken(accessToken).getUserName();
            var userUUID = jwtUtil.getClaimsFromAccessToken(accessToken).getUserUUID();

            AuthCacheData authCacheData = cacheUtil.getBucket(AUTH_CACHE_DATA_PREFIX + userName + ":" + userUUID);
            if (authCacheData == null) throw new AuthException(TOKEN_EXPIRED_MESSAGE, TOKEN_EXPIRED_CODE, 406);

            var publicKey = KeyFactory.getInstance(RSA).generatePublic(
                    new X509EncodedKeySpec(decodeFromString(authCacheData.getPublicKey()))
            );

            jwtUtil.verifyToken(accessToken, (RSAPublicKey) publicKey);

            if (jwtUtil.isTokenExpired(authCacheData.getAccessTokenClaimsSet().getExpirationTime())) {
                throw new AuthException(TOKEN_EXPIRED_MESSAGE, TOKEN_EXPIRED_CODE, 406);
            }

            return AuthPayloadResponse.of(userName, userUUID);
        } catch (AuthException  ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error(String.valueOf(ex));
            throw new AuthException(USER_UNAUTHORIZED_MESSAGE, USER_UNAUTHORIZED_CODE, 401);
        }
    }
}