package az.auth.msauth.service;

import az.auth.msauth.dao.entity.UserEntity;
import az.auth.msauth.dao.repository.UserRepository;
import az.auth.msauth.exception.BadRequestException;
import az.auth.msauth.model.request.UserRequest;
import az.auth.msauth.model.response.TokenResponse;
import az.auth.msauth.util.SecurityUtil;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

import static az.auth.msauth.util.SecurityUtil.checkPassword;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final TokenService tokenService;
    private final SecurityUtil securityUtil;

    @Value("${jwt.refreshToken.expiration.count}")
    private int refreshTokenExpirationCount;

    public void registerUser(UserRequest userRequest) {

        userRepository.findByName(userRequest.getUserName())
                .ifPresent(
                        exception ->
                        {
                            throw new BadRequestException("There is user with this name", "GENERAL_EXCEPTION");
                        }
                );

        userRepository.save(
                UserEntity.builder()
                        .name(userRequest.getUserName())
                        .password(securityUtil.encoder().encode(userRequest.getPassword()))
                        .userUuid(UUID.randomUUID().toString())
                        .build()
        );
    }

    public TokenResponse login(UserRequest userRequest) {

        UserEntity user = userRepository.findByName(userRequest.getUserName())
                .orElseThrow(
                        () -> new BadRequestException("There is no user with this name", "GENERAL_EXCEPTION")
                );

        if (checkPassword(userRequest.getPassword(), user.getPassword())) {
            return tokenService.generateToken(user.getName(), user.getUserUuid(), refreshTokenExpirationCount);
        } else {
            throw new BadRequestException("There is error with password matches", "GENERAL_EXCEPTION");
        }
    }
}
