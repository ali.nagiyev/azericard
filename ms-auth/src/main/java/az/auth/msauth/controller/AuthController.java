package az.auth.msauth.controller;

import az.auth.msauth.model.request.VerifyTokenRequest;
import az.auth.msauth.model.response.AuthPayloadResponse;
import az.auth.msauth.service.TokenService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class AuthController {

    TokenService tokenService;

    @PostMapping("/verify")
    public AuthPayloadResponse verifyAccessToken(
            @RequestBody VerifyTokenRequest request
    ) {
        return tokenService.validateToken(request.getAccessToken());
    }
}
