package az.auth.msauth.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class SecurityUtil {

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    public static boolean checkPassword(String password, String encryptedPassword){
        return new BCryptPasswordEncoder().matches(password, encryptedPassword);
    }
}
