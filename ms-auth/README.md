<h1 align="center"> ms.auth </h1> <br>

## Table of Contents

- [Introduction](#Introduction)
- [How to run locally](#How-to-run-locally)
- [Tech stack](#Tech-stack)
- [Swagger](#Swagger-link)

## Introduction

This app's responsibility is to register users and register them. You can register user unique name

## How to run locally

shell script
$ java -jar ms.otp.jar


## Tech stack
1. Spring Boot
2. Java 11
3. Redis

## Swagger link
[ms.auth](http://localhost:8001/swagger-ui/index.html#/
