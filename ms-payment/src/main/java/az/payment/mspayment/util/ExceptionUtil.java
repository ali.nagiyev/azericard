package az.payment.mspayment.util;

import lombok.NoArgsConstructor;

import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class ExceptionUtil {

    public static Optional<RuntimeException> handleAndReturnRuntimeException(Runnable runnable) {

        try {
            runnable.run();
        } catch (RuntimeException e) {
            return Optional.of(e);
        }
        return Optional.empty();
    }
}
