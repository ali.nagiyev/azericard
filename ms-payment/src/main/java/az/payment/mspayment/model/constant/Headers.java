package az.payment.mspayment.model.constant;

public interface Headers {
    String AUTHORIZATION = "Authorization";
    String LANGUAGE = "Accept-Language";
}
