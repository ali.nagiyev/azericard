package az.payment.mspayment.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Exceptions {

    INSUFFICIENT_FUNDS("INSUFFICIENT_FUNDS",
            "Balansınızda kifayət qədər vəsait yoxdur. Lütfən balansınızı artırın və yenidən cəhd edin.",
            "You have insufficient funds on the balance. Please top-up your balance and try again.",
            "У вас недостаточно средств на балансе. Пожалуйста, пополните баланс и попытайтесь ещё раз."
    ),

    INSUFFICIENT_STOCK("INSUFFICIENT_STOCK",
            "Stock'da kifayət qədər vəsait yoxdur",
            "You have insufficient funds on the stock",
            "У вас недостаточно средств на сток"
    ),

    CLIENT_EXCEPTION("CLIENT_FAILED",
            "Sistem xətası.",
            "System error.",
            "Сбой системы."
    );

    private final String code;
    private final String translationAz;
    private final String translationEn;
    private final String translationRu;

    public static String getExceptionTranslation(String code, Language language) {
        return Arrays.stream(Exceptions.values())
                .filter(exception -> exception.getCode().equals(code))
                .findFirst()
                .map(exception -> getTranslationByLanguage(exception, language))
                .orElse(getTranslationByLanguage(CLIENT_EXCEPTION, language));
    }

    private static String getTranslationByLanguage(Exceptions exception, Language language) {
        switch (language) {
            case AZE:
                return exception.getTranslationAz();
            case ENG:
                return exception.getTranslationEn();
            default:
                return exception.getTranslationRu();
        }
    }
}
