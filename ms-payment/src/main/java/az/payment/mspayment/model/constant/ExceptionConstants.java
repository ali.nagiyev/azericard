package az.payment.mspayment.model.constant;

public interface ExceptionConstants {
    String UNEXPECTED_EXCEPTION_CODE = "UNEXPECTED_EXCEPTION";
    String CLIENT_EXCEPTION_CODE = "CLIENT_FAILED";
    String USER_UNAUTHORIZED_CODE = "USER_UNAUTHORIZED";
    String USER_UNAUTHORIZED_MESSAGE = "User unauthorized";
    String INSUFFICIENT_FUNDS_CODE = "INSUFFICIENT_FUNDS";
    String INSUFFICIENT_STOCK_CODE = "INSUFFICIENT_STOCK";
    String INSUFFICIENT_FUNDS_MESSAGE = "You don't have sufficient funds for this operation";
    String INSUFFICIENT_FUNDS_CONTAINED_WORD = "There is insufficient balance";
    String INSUFFICIENT_STOCK_WORD = "There is no this count in stock";
}
