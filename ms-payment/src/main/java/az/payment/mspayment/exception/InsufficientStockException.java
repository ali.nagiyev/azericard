package az.payment.mspayment.exception;

import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class InsufficientStockException extends RuntimeException {

    String code;

    public InsufficientStockException(String message, String code) {
        super(message);
        this.code = code;
    }
}
