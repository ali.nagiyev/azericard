package az.payment.mspayment.config.logging;

import feign.Logger;
import feign.Request;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomLogging extends Logger {

    @Override
    protected void log(String configKey, String format, Object... args) {

        logger.info(String.format(methodTag(configKey).replace("#", ".") + format, args));
    }

    @Override
    protected void logRequest(String configKey, Level logLevel, Request request) {

        var split = request.url().split("\\?");

        log(configKey, "url: %s  %s", split[0], request.httpMethod().name());

        if (split.length == 2) {
            log(configKey, "request parameters: { %s }", split[1].replace("&", ", "));
        }

        if (request.body() != null) {

            var bodyText = request.charset() != null
                    ? new String(request.body(), request.charset())
                    : "Binary data";

            log(configKey, "request body: %s", bodyText);
        }
    }
}
