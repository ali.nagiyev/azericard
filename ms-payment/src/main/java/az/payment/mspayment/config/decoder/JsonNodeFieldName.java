package az.payment.mspayment.config.decoder;

interface JsonNodeFieldName {

    String MESSAGE = "message";
    String CODE = "code";
    String DATA = "data";
}
