package az.payment.mspayment.client;

import az.payment.mspayment.model.request.VerifyTokenRequest;
import az.payment.mspayment.model.response.AuthPayloadResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "ms-auth",
        url = "localhost:8001/"
)
public interface AuthClient {

    @PostMapping("/api/auth/verify")
    AuthPayloadResponse verifyAccessToken(@RequestBody VerifyTokenRequest request);
}
