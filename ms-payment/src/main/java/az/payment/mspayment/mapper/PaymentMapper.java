package az.payment.mspayment.mapper;

import az.payment.mspayment.dao.entity.PaymentEntity;
import az.payment.mspayment.model.response.PaymentResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PaymentMapper {

    PaymentEntity toPaymentEntity(PaymentResponse paymentResponse);

    PaymentResponse toPaymentResponse(PaymentEntity paymentEntity);

    List<PaymentEntity> toPaymentEntityList(List<PaymentResponse> paymentResponse);

    List<PaymentResponse> toPaymentResponseList(List<PaymentEntity> paymentEntity);
}
