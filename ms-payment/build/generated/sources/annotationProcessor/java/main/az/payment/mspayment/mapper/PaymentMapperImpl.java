package az.payment.mspayment.mapper;

import az.payment.mspayment.dao.entity.PaymentEntity;
import az.payment.mspayment.dao.entity.PaymentEntity.PaymentEntityBuilder;
import az.payment.mspayment.model.response.PaymentResponse;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-28T20:21:28+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.3.jar, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class PaymentMapperImpl implements PaymentMapper {

    @Override
    public PaymentEntity toPaymentEntity(PaymentResponse paymentResponse) {
        if ( paymentResponse == null ) {
            return null;
        }

        PaymentEntityBuilder paymentEntity = PaymentEntity.builder();

        return paymentEntity.build();
    }

    @Override
    public PaymentResponse toPaymentResponse(PaymentEntity paymentEntity) {
        if ( paymentEntity == null ) {
            return null;
        }

        PaymentResponse paymentResponse = new PaymentResponse();

        return paymentResponse;
    }

    @Override
    public List<PaymentEntity> toPaymentEntityList(List<PaymentResponse> paymentResponse) {
        if ( paymentResponse == null ) {
            return null;
        }

        List<PaymentEntity> list = new ArrayList<PaymentEntity>( paymentResponse.size() );
        for ( PaymentResponse paymentResponse1 : paymentResponse ) {
            list.add( toPaymentEntity( paymentResponse1 ) );
        }

        return list;
    }

    @Override
    public List<PaymentResponse> toPaymentResponseList(List<PaymentEntity> paymentEntity) {
        if ( paymentEntity == null ) {
            return null;
        }

        List<PaymentResponse> list = new ArrayList<PaymentResponse>( paymentEntity.size() );
        for ( PaymentEntity paymentEntity1 : paymentEntity ) {
            list.add( toPaymentResponse( paymentEntity1 ) );
        }

        return list;
    }
}
