package az.product.msproduct.mapper;

import az.product.msproduct.dao.entity.ProductEntity;
import az.product.msproduct.model.dto.ProductDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-28T20:36:06+0400",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.3.jar, environment: Java 17.0.8 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDto toProductDto(ProductEntity productEntity) {
        if ( productEntity == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setName( productEntity.getName() );
        productDto.setPrice( productEntity.getPrice() );
        productDto.setStockCount( productEntity.getStockCount() );
        productDto.setProductUuid( productEntity.getProductUuid() );

        return productDto;
    }

    @Override
    public List<ProductDto> toProductDtoList(List<ProductEntity> productEntityList) {
        if ( productEntityList == null ) {
            return null;
        }

        List<ProductDto> list = new ArrayList<ProductDto>( productEntityList.size() );
        for ( ProductEntity productEntity : productEntityList ) {
            list.add( toProductDto( productEntity ) );
        }

        return list;
    }
}
