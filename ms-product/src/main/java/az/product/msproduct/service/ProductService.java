package az.product.msproduct.service;

import az.product.msproduct.dao.entity.ProductEntity;
import az.product.msproduct.dao.repository.ProductRepository;
import az.product.msproduct.exception.BadRequestException;
import az.product.msproduct.mapper.ProductMapper;
import az.product.msproduct.model.dto.ProductDto;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ProductService {

    ProductRepository productRepository;
    ProductMapper productMapper;

    public List<ProductDto> getAllAvailableProducts() {

        return productRepository.findAllByStockCountIsNot(0).stream()
                .map(productMapper::toProductDto)
                .collect(toList());
    }

    public ProductDto getProductById(Long id) {

        return productRepository.findById(id)
                .map(productMapper::toProductDto)
                .orElseThrow(
                        () -> new BadRequestException("There is no product with this id", "GENERAL_EXCEPTION")
                );
    }

    public Map<String, ProductDto> getProductByUuid(List<String> uuid) {

        return productRepository.findByProductUuidIn(uuid).stream()
                .map(productMapper::toProductDto)
                .collect(
                        Collectors.toMap(
                                ProductDto::getProductUuid, p -> ProductDto.of(p.getName(), p.getPrice(), p.getStockCount(), p.getProductUuid())
                        )
                );
    }

    @Transactional
    public void checkAndUpdateProductCount(String productUuid, Integer productCount) {

        ProductEntity productEntity = productRepository.findByProductUuid(productUuid)
                .orElseThrow(
                        () -> new BadRequestException("There is no product with this uuid", "GENERAL_EXCEPTION")
                );

        if (productCount > productEntity.getStockCount())
            throw new BadRequestException("There is no this count in stock", "GENERAL_EXCEPTION");

        productEntity.setStockCount(productEntity.getStockCount() - productCount);
        productRepository.save(
                productEntity
        );
    }

    public void reverseProduct(String productUuid, Integer productCount) {
        ProductEntity productEntity = productRepository.findByProductUuid(productUuid)
                .orElseThrow(
                        () -> new BadRequestException("There is no product with this uuid", "GENERAL_EXCEPTION")
                );

        productEntity.setStockCount(productEntity.getStockCount() + productCount);
        productRepository.save(
                productEntity
        );
    }
}
