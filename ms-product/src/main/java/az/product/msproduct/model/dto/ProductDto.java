package az.product.msproduct.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

import static lombok.AccessLevel.PRIVATE;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class ProductDto {

    String name;
    BigDecimal price;
    Integer stockCount;
    String productUuid;

}
